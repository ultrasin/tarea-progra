package cr.ac.una.cooperativa;

import cr.ac.una.cooperativa.util.FlowController;
import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;
import static javafx.application.Application.launch;

/**
 * JavaFX App
 */
public class App extends Application {

     @Override
    public void start(Stage stage) throws IOException {
        FlowController.getInstance().InitializeFlow(stage, null);
        stage.setTitle("Una Escuela Cooperativa");
        FlowController.getInstance().goViewInWindow("LandingView");
    }

    public static void main(String[] args) {
        launch();
    }
}