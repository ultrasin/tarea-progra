package cr.ac.una.cooperativa.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativa.service.CuentaService;
import cr.ac.una.cooperativa.util.Formato;
import cr.ac.una.cooperativa.util.Mensaje;
import cr.ac.una.cooperativa.util.Respuesta;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import cr.ac.una.cooperativa.model.CuentaDto;

/**
 *
 * @author Lapiton
 */
public class ProfesorViewController extends Controller implements Initializable {

    @FXML
    private AnchorPane root;
    
    @FXML
    private JFXTextField txtIdCuentaAhorro;
    @FXML
    private JFXTextField txtNombreCuentaAhorro;
    @FXML
    private JFXTextField txtMontoCuentaAhorro;
    @FXML
    private JFXButton btnNuevaCuenta;
    @FXML
    private JFXButton btnGuardarCuenta;
    @FXML
    private JFXButton btnEliminarCuenta;
    
    /**
     * 
     */
    CuentaDto cuenta;
    List<Node> requeridos = new ArrayList<>();
    
    /**
     * 
     * @param url
     * @param rb 
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtNombreCuentaAhorro.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtMontoCuentaAhorro.setTextFormatter(Formato.getInstance().cedulaFormat(15));
        cuenta = new CuentaDto();
        nuevaCuenta();
        indicarRequeridos();
    }
    
    /**
     * 
     */
    public void indicarRequeridos(){
        requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombreCuentaAhorro, txtMontoCuentaAhorro));
    }
    /**
     * 
     * @return campos requeridos al ingresar tipo de cuenta nuevos
     */
    public String validarRequeridos(){
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos){
            if (node instanceof JFXTextField && ((JFXTextField)node).getText().isEmpty()){
                if (validos){
                        invalidos += ((JFXTextField)node).getPromptText();}
                else {
                    invalidos += "," + ((JFXTextField)node).getPromptText();}
                validos = false;}
        }
        if (validos) {
            return "";}
        else{
            return "Campos requeridos o con problemas de formatos["+invalidos+"].";}
    }
    
    private void bindTipoCuenta(boolean nuevo){
        if(!nuevo){
            txtIdCuentaAhorro.textProperty().bind(cuenta.cuentaId);
        }
        txtNombreCuentaAhorro.textProperty().bindBidirectional(cuenta.cuentaNombre);
        txtMontoCuentaAhorro.textProperty().bindBidirectional(cuenta.cuentaMonto);
    }
    
    private void unbindTipoCuenta(){
        txtIdCuentaAhorro.textProperty().unbind();
        txtNombreCuentaAhorro.textProperty().unbindBidirectional(cuenta.cuentaNombre);
        txtMontoCuentaAhorro.textProperty().unbindBidirectional(cuenta.cuentaMonto);
    }
    
    public void nuevaCuenta(){
        unbindTipoCuenta();
        cuenta = new CuentaDto();
        bindTipoCuenta(true);
        txtIdCuentaAhorro.clear();
        txtIdCuentaAhorro.requestFocus();
    }
    
    private void cargarCuenta(Long id) {
        CuentaService service = new CuentaService();
        Respuesta respuesta = service.getCuenta(id);
        
        if (respuesta.getEstado()){
            unbindTipoCuenta();
            cuenta = (CuentaDto)respuesta.getResultado("Cuenta");
            bindTipoCuenta(false);
            validarRequeridos();
        } else{
            new Mensaje().showModal(Alert.AlertType.ERROR, "Cargar cuenta", getStage(), respuesta.getMensaje());
        }
    }
    
    @FXML
    private void onActionBtnGuardarCuenta(ActionEvent event) {
        try {
            String invalidados = validarRequeridos();
            if (!invalidados.isEmpty()){
                new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar el tipo de cuenta", getStage(), invalidados);
            } else{
                
                CuentaService service = new CuentaService();
                Respuesta respuesta = service.guardarCuenta(cuenta);
                
                if (!respuesta.getEstado()){
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar el tipo de cuenta", getStage(), respuesta.getMensaje());
                } else {
                    unbindTipoCuenta();
                    cuenta = (CuentaDto) respuesta.getResultado("Cuenta");
                    bindTipoCuenta(false);
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Guardar el tipo de cuenta", getStage(), "Tipo de cuenta guardada exitosamente.");
                }
            } 
        } 
        catch (Exception ex){
            Logger.getLogger(ProfesorViewController.class.getName()).log(Level.SEVERE, "Error guardando el tipo de cuenta", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Guardar el tipo de cuenta", getStage(), "Ocurrió un error guardando el tipo de cuenta");
        }
    }
    
    @FXML
    private void onActionBtnNuevaCuenta(ActionEvent event) {
        if (new Mensaje().showConfirmation("Limpiar cuenta", getStage(), "¿Está seguro de limpiar el registro?")){
            nuevaCuenta();
        }
    }

    @FXML
    private void onActionBtnEliminarCuenta(ActionEvent event) {
        try {
            if (cuenta.getCuentaId() == null){
                new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar el tipo de cuenta", getStage(), "Debe cargar una cuenta para poderla eliminar.");}
            else{
                CuentaService service = new CuentaService();
                Respuesta respuesta = service.eliminarCuenta(cuenta.getCuentaId());
                
                if (!respuesta.getEstado()){
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar el tipo de cuenta", getStage(), respuesta.getMensaje());
                } else {
                    new Mensaje().showModal(Alert.AlertType.INFORMATION, "Eliminar el tipo de cuenta", getStage(), "Tipo de cuenta eliminada corractamente");
                    nuevaCuenta();
                }
            }
        } 
        catch (Exception ex){
            Logger.getLogger(ProfesorViewController.class.getName()).log(Level.SEVERE, "Error eliminando tipo de cuenta", ex);
            new Mensaje().showModal(Alert.AlertType.ERROR, "Eliminar tipo de cuenta", getStage(), "Ocurrió un error eliminando la cuenta");
        }
    }
    
    @FXML
    private void onKeyPressedTxtIdCuentaAhorro(KeyEvent event) {
        if(event.getCode() == KeyCode.ENTER && !txtIdCuentaAhorro.getText().isEmpty()){
            cargarCuenta(Long.valueOf(txtIdCuentaAhorro.getText()));
        }
    }
    
    @Override
    public void initialize() {
        
    }
}
