package cr.ac.una.cooperativa.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Lapiton
 */
public class BusquedaViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnBuscar;
    @FXML
    private JFXButton btnRegresar;
    @FXML
    private AnchorPane BusquedaView;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @Override
    public void initialize() {
        
    }
    
}
