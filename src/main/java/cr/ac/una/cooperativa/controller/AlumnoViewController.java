package cr.ac.una.cooperativa.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativa.model.AlumnoDto;
import cr.ac.una.cooperativa.util.BindingUtils;
import cr.ac.una.cooperativa.util.FlowController;
import cr.ac.una.cooperativa.util.Formato;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Ale
 */
public class AlumnoViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnCapturar;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtGrado;
    @FXML
    private JFXTextField txtEdad;
    @FXML
    private JFXRadioButton rdbMasculino;
    @FXML
    private ToggleGroup tggGenero;
    @FXML
    private JFXRadioButton rdbFemenino;
    @FXML
    private JFXButton btnGuardar;
    @FXML
    private JFXButton btnSummit;
    @FXML
    private JFXButton btnRegresar;
    @FXML
    private JFXTextField txtPApellido;
    @FXML
    private JFXTextField txtSApellido;
   
    AlumnoDto alumno;
    List<Node> requeridos = new ArrayList<>();
    ListView <ImageView> img;
    
    boolean imgCache;
    @FXML
    private AnchorPane root;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*rdbMasculino.setUserData("M");
        rdbFemenino.setUserData("F");
        txtNombre.setTextFormatter(Formato.getInstance().letrasFormat(30));
        txtPApellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtSApellido.setTextFormatter(Formato.getInstance().letrasFormat(15));
        txtGrado.setTextFormatter(Formato.getInstance().letrasFormat(10));
        txtEdad.setTextFormatter(Formato.getInstance().cedulaFormat(2));
        nuevoAlumno();
        indicarRequeridos();*/
    }
    
    public void indicarRequeridos() {
        /*requeridos.clear();
        requeridos.addAll(Arrays.asList(txtNombre, txtPApellido, txtSApellido, txtEdad));*/
    }
    
    /*public String validarRequeridos(){
        Boolean validos = true;
        String invalidos = "";
        for (Node node : requeridos){
            if (node instanceof JFXTextField && ((JFXTextField)node).getText().isEmpty()){
                if (validos){
                        invalidos += ((JFXTextField)node).getPromptText();}
                else {
                    invalidos += "," + ((JFXTextField)node).getPromptText();}
                validos = false;}
        }
        if (validos) {
            return "";}
        else{
            return "Campos requeridos o con problemas de formatos["+invalidos+"].";}
    }*/
    
    /*private void bindNuevoAlumno(boolean nuevo){
        /*if(!nuevo){
            //La BD agregara un folio
        }
        txtNombre.textProperty().bindBidirectional(alumno.aluNombre);
        txtPApellido.textProperty().bindBidirectional(alumno.aluPapellido);
        txtSApellido.textProperty().bindBidirectional(alumno.aluSapellido);
        txtGrado.textProperty().bindBidirectional(alumno.aluGrado);
        txtEdad.textProperty().bindBidirectional(alumno.aluEdad);
        BindingUtils.bindToggleGroupToProperty(tggGenero, alumno.aluGenero);
    }
    
    private void unbindNuevoAlumno(){
        txtNombre.textProperty().unbindBidirectional(alumno.aluNombre);
        txtPApellido.textProperty().unbindBidirectional(alumno.aluPapellido);
        txtSApellido.textProperty().unbindBidirectional(alumno.aluSapellido);
        txtGrado.textProperty().unbindBidirectional(alumno.aluGrado);
        txtEdad.textProperty().unbindBidirectional(alumno.aluEdad);
        BindingUtils.unbindToggleGroupToProperty(tggGenero, alumno.aluGenero);
    }
    
    private void nuevoAlumno() {
        unbindNuevoAlumno();
        alumno = new AlumnoDto();
        bindNuevoAlumno(true);
    }*/

    @FXML
    private void onActionBtnCapturar(ActionEvent event) {
        //test no funcional
        
//        try {
//            BufferedImage image = webcam.getImage();
//            //nombre y formato de la imagen de salida
//            ImageIO.write(image, "PNG", new File("Img_Asociado.png"));
//        } catch (IOException ex) {
//            Logger.getLogger(PanelCamara.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        webcam.close();
//        setVisible(false);
//        }
    }

    @FXML
    private void onActionBtnGuardar(ActionEvent event) {
    }

    @FXML
    private void onActionSummit(ActionEvent event) {
    }

    @FXML
    private void onActionBtnRegresar(ActionEvent event) {
        FlowController.getInstance().goMenu();
    }
    
    @Override
    public void initialize() {
        
    }
    
}
