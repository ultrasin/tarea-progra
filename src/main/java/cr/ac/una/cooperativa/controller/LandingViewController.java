package cr.ac.una.cooperativa.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.cooperativa.util.FlowController;
import cr.ac.una.cooperativa.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Lapiton
 */
public class LandingViewController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtUsuario;
    @FXML
    private JFXTextField txtClave;
    @FXML
    private JFXButton btnReset;
    @FXML
    private JFXButton btnEntrar;
    @FXML
    private ImageView imgFondo;
    @FXML
    private AnchorPane root;
    @FXML
    private StackPane stackpane;
    @FXML
    private VBox vboxLogin;
    @FXML
    private VBox vbTitulo;
    @FXML
    private JFXButton btnAlumno;
    @FXML
    private JFXButton btnProfesor;
    @FXML
    private Label labelInfo;
    
    @FXML
    private void onActionBtnReset(ActionEvent event) {
        btnAlumno.setDisable(false);
        btnProfesor.setDisable(false);
        labelInfo.setText("");
        txtUsuario.clear();
        txtClave.clear();
    }
    
    @FXML
    private void onActionBtnEntrar(ActionEvent event) {
        
        try {
                if (txtUsuario.getText() == null || txtUsuario.getText().isEmpty()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de usuario",
                    (Stage) btnEntrar.getScene().getWindow(),"Es necesario digitar un usuario para continuar");}
            
                else if (txtClave.getText() == null || txtClave.getText().isEmpty()) {
                    new Mensaje().showModal(Alert.AlertType.ERROR, "Validación de clave",
                    (Stage) btnEntrar.getScene().getWindow(), "Es necesario digitar una contraseña para continuar");}
                
                else if ((btnProfesor.isDisable() && btnAlumno.isDisable())){ 
                    FlowController.getInstance().goMantenimiento(); 
                    ((Stage) btnEntrar.getScene().getWindow()).close();}
                
                else if (btnAlumno.isDisable()){ 
                    FlowController.getInstance().goAlumno(); 
                    ((Stage) btnEntrar.getScene().getWindow()).close();}
                
                else if (btnProfesor.isDisable()){ 
                    FlowController.getInstance().goProfesor(); 
                    ((Stage) btnEntrar.getScene().getWindow()).close();}
               
            }     
        catch (Exception ex) {
            Logger.getLogger(LandingViewController.class.getName()).log(Level.SEVERE, "Error ingresando.", ex);
                }
    }
    
    @FXML
    private void onActionBtnAlumno(ActionEvent event) {
        btnAlumno.setDisable(true);
        txtClave.setDisable(false);
        txtUsuario.setDisable(false);
        btnReset.setDisable(false);
        mostrarAdmin();
    }
    
    @FXML
    private void onActionBtnProfesor(ActionEvent event) {
        btnProfesor.setDisable(true);
        txtClave.setDisable(false);
        txtUsuario.setDisable(false);
        btnReset.setDisable(false);
        mostrarAdmin();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imgFondo.fitHeightProperty().bind(root.heightProperty());
        imgFondo.fitWidthProperty().bind(root.widthProperty());
        btnReset.setDisable(true);
    }    
    
    @Override
    public void initialize() {
    }
    
    public void mostrarAdmin(){
        if (btnAlumno.isDisable()&& btnProfesor.isDisable()){
            labelInfo.setText("[Eres administrador]");}
         else if (btnAlumno.isDisabled()){
            labelInfo.setText("Digite su información\n"
                    + "de alumno");}
         else if (btnProfesor.isDisabled()){
            labelInfo.setText("Digite su información\n"
                    + "de profesor");}
        } 
}
