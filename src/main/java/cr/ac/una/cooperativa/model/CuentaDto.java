package cr.ac.una.cooperativa.model;

import javafx.beans.property.SimpleStringProperty;

public class CuentaDto {
    public SimpleStringProperty cuentaId;
    public SimpleStringProperty cuentaNombre;
    public SimpleStringProperty cuentaMonto;
    private Boolean modificado;
    
    public CuentaDto(){
    
        this.modificado = false;
        this.cuentaId = new SimpleStringProperty();
        this.cuentaNombre = new SimpleStringProperty();
        this.cuentaMonto = new SimpleStringProperty();
    }
    
    public Long getCuentaId() {
        if(cuentaId.get()!=null && !cuentaId.get().isEmpty())
            return Long.valueOf(cuentaId.get());
        else
            return null;
    }
    
    public void setCuentaId(Long cuentaId) {
        this.cuentaId.set(cuentaId.toString());
    }
    
    public String getCuentaNombre() {
        return cuentaNombre.get();
    }

    public void setCuentaNombre(String cuentaNombre) {
        this.cuentaNombre.set(cuentaNombre);
    }

    public String getCuentaMonto() {
        return cuentaMonto.get();
    }

    public void setCuentaMonto(String cuentaMonto) {
        this.cuentaMonto.set(cuentaMonto);
    }
    
    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }
    
    @Override
    public String toString() {
        return "CuentaDto{" + "cuentaId=" + cuentaId + ", cuentaNombre=" + cuentaNombre + ", cuentaMonto=" + cuentaMonto + '}';
    }
}
