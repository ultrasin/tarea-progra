package cr.ac.una.cooperativa.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ale
 */
public class AlumnoDto {
    
    @XmlTransient
    public SimpleStringProperty aluNombre;
    @XmlTransient
    public SimpleStringProperty aluPapellido;
    @XmlTransient
    public SimpleStringProperty aluSapellido;
    @XmlTransient
    public SimpleStringProperty aluGrado;
    @XmlTransient
    public SimpleStringProperty aluEdad;
    @XmlTransient
    public ObjectProperty<String> aluGenero;
    
    @XmlTransient
    private Boolean modificado;
    
    public AlumnoDto() {
        this.modificado = false;
        this.aluNombre = new SimpleStringProperty();
        this.aluPapellido = new SimpleStringProperty();
        this.aluSapellido = new SimpleStringProperty();
        this.aluGrado = new SimpleStringProperty();
        this.aluEdad = new SimpleStringProperty();
        this.aluGenero = new SimpleObjectProperty("M");
    }

    public String getAluNombre() {
        return aluNombre.get();
    }

    public void setAluNombre(String aluNombre) {
        this.aluNombre.set(aluNombre);
    }

    public String getAluPapellido() {
        return aluPapellido.get();
    }

    public void setAluPapellido(String aluPapellido) {
        this.aluPapellido.set(aluPapellido);
    }

    public String getAluSapellido() {
        return aluSapellido.get();
    }

    public void setAluSapellido(String aluSapellido) {
        this.aluSapellido.set(aluSapellido);
    }

    public String getAluGrado() {
        return aluGrado.get();
    }

    public void setAluGrado(String aluGrado) {
        this.aluGrado.set(aluGrado);
    }

    public String getAluEdad() {
        return aluEdad.get();
    }

    public void setAluEdad(String aluEdad) {
        this.aluEdad.set(aluEdad);
    }

    public String getAluGenero() {
        return aluGenero.get();
    }

    public void setEmpGenero(String aluGenero) {
        this.aluGenero.set(aluGenero);
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "AlumnoDto{" + "aluNombre=" + aluNombre + ", aluPapellido=" + aluPapellido + ", aluSapellido=" + aluSapellido + ", aluGrado=" + aluGrado + ", aluEdad=" + aluEdad + ", aluGenero=" + aluGenero + ", modificado=" + modificado + '}';
    }
    
    
}
