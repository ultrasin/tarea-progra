package cr.ac.una.cooperativa.service;

import cr.ac.una.cooperativa.model.AlumnoDto;
import cr.ac.una.cooperativa.util.Respuesta;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ale
 */
public class AlumnoService {
    
    public Respuesta guardarAlumno(AlumnoDto alumno) {
        try {
            return new Respuesta(true, "", "", "Alumno", new AlumnoDto());
        } catch (Exception ex) {
            Logger.getLogger(AlumnoService.class.getName()).log(Level.SEVERE, "Error guardando el alumno.", ex);
            return new Respuesta(false, "Error guardando al alumno.", "guardarAlumno " + ex.getMessage());
        }
    }
    
    public Respuesta guardarImgAlumno(AlumnoDto imgAlumno) { //Agregar metodos en el AlumnoDto y lo de llamar a la camara
        try {
            return new Respuesta(true, "", "", "ImgAlumno", new AlumnoDto());
        } catch (Exception ex) {
            Logger.getLogger(AlumnoService.class.getName()).log(Level.SEVERE, "Error guardando la imagen del alumno.", ex);
            return new Respuesta(false, "Error guardando la imagen del alumno.", "guardarAlumno " + ex.getMessage());
        }
    }
    
}
