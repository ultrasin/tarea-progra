package cr.ac.una.cooperativa.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import cr.ac.una.cooperativa.model.CuentaDto;
import cr.ac.una.cooperativa.util.Respuesta;
/**
 *
 * @author Lapiton
 */
public class CuentaService {
    
    public Respuesta getCuenta(Long id) {
        try {
            CuentaDto cuenta = new CuentaDto();
            return new Respuesta(true, "", "", "Cuenta", cuenta);
        } catch (Exception ex) {
            Logger.getLogger(CuentaService.class.getName()).log(Level.SEVERE, "Error obteniendo el tipo de cuenta [" + id + "]", ex);
            return new Respuesta(false, "Error obteniendo el tipo de cuenta.", "getCuenta " + ex.getMessage());
        }
    }
    
    public Respuesta getCuentas(String nombre) {
        try {
            List<CuentaDto> cuentas = new ArrayList<>();
            return new Respuesta(true, "", "", "Cuentas", cuentas);
        } catch (Exception ex) {
            Logger.getLogger(CuentaService.class.getName()).log(Level.SEVERE, "Error obteniendo las cuentas.", ex);
            return new Respuesta(false, "Error obteniendo las cuentas.", "getCuentas " + ex.getMessage());
        }
    }
    
    public Respuesta guardarCuenta(CuentaDto cuenta) {
        try {
            return new Respuesta(true, "", "", "Cuenta", new CuentaDto());
        } catch (Exception ex) {
            Logger.getLogger(CuentaService.class.getName()).log(Level.SEVERE, "Error guardando el tipo de cuenta.", ex);
            return new Respuesta(false, "Error guardando el tipo de cuenta.", "guardarCuenta " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarCuenta(Long id) {
        try {
            return new Respuesta(true, "", "");
        } catch (Exception ex) {
            Logger.getLogger(CuentaService.class.getName()).log(Level.SEVERE, "Error eliminando el tipo de cuenta.", ex);
            return new Respuesta(false, "Error eliminando el tipo de cuenta.", "eliminarCuenta " + ex.getMessage());
        }
    }
    
}
